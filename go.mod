module calc

go 1.12

require (
	github.com/antlr/antlr4 v0.0.0-20190223165740-dade65a895c2
	github.com/ymotongpoo/calc v0.0.0
)

replace github.com/ymotongpoo/calc v0.0.0 => ./
