PROGRAM      = calc
ANTLR_SRC    = parser/Calc.g4
ANTLR_OBJS   = parser/*.go parser/*.interp parser/*.tokens 

.PHONY: all clean

all: $(PROGRAM)

$(PROGRAM): main.go antlr
	go build -o $(PROGRAM)

antlr: $(ANTLR_SRC)
	antlr4 -Dlanguage=Go $(ANTLR_SRC)

clean:
	rm -rf $(PROGRAM) $(ANTLR_OBJS)