// followed this blog entry
// http://ikawaha.hateblo.jp/entry/2017/03/19/002257

package main

import (
	"fmt"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"

	"github.com/ymotongpoo/calc/parser"
)

func main() {
	input := antlr.NewInputStream("1+(2+3)*4-5")

	lexer := parser.NewCalcLexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)
	p := parser.NewCalcParser(stream)

	listener := NewCalcListener()
	p.AddParseListener(listener)

	tree := p.Prog()
	fmt.Println(tree.ToStringTree([]string{}, p))
	fmt.Printf("---> %+v\n", listener.Stack)
}

type CalcListener struct {
	*parser.BaseCalcListener
	Stack []int
	Error error
}

func NewCalcListener() *CalcListener {
	return &CalcListener{Stack: []int{}}
}

func (l *CalcListener) ExitExpr(ctx *parser.ExprContext) {
	if l.Error != nil {
		return
	}
	if ctx.GetExpr_() != nil {
		return
	}
	if ctx.GetOp() != nil {
		rhs := l.Stack[len(l.Stack)-1]
		l.Stack = l.Stack[:len(l.Stack)-1]
		lhs := l.Stack[len(l.Stack)-1]
		l.Stack = l.Stack[:len(l.Stack)-1]

		switch ctx.GetOp().GetText() {
		case "*":
			l.Stack = append(l.Stack, lhs*rhs)
		case "/":
			l.Stack = append(l.Stack, lhs/rhs)
		case "+":
			l.Stack = append(l.Stack, lhs+rhs)
		case "-":
			l.Stack = append(l.Stack, lhs-rhs)
		}
	}
	if ctx.GetAtom() != nil {
		i, err := strconv.Atoi(ctx.GetAtom().GetText())
		if err != nil {
			l.Error = fmt.Errorf("invalid value: %v", err)
		}
		l.Stack = append(l.Stack, i)
		return
	}
}
